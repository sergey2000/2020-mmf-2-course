from datetime import datetime
import timeit
import time 

def main():
    file = "out1000.csv"
    arrFormat = '%Y-%m-%d %H:%M:%S'

    ResourseID = 689
    StartTime = "2021-06-01T21:00:00.000Z"
    StartTime = StartTime.replace('T', ' ')[:StartTime.rfind('.')]
    StartTime = datetime.strptime(StartTime, arrFormat)
    StartTime = int(time.mktime(StartTime.timetuple())) // 100

    EndTime = "2023-09-07T21:00:00.000Z"
    EndTime = EndTime.replace('T', ' ')[:EndTime.rfind('.')]
    EndTime = datetime.strptime(EndTime, arrFormat)
    EndTime = int(time.mktime(EndTime.timetuple())) // 100

    arr = parseInput(readFile(file), arrFormat)
    return [arr, ResourseID, StartTime, EndTime]



def readFile(file):
    f = open(file)
    
    arr = f.readFile()

    f.close()

    return arr

def parseString(str):
    return str.split(",")

def printArray(arr):
    for i in arr:
        print(i)

def parseInput(arr, arrFormat):

    dict = {}



    arr = arr.split('\n')
        
    for i in range(1, len(arr) - 1):

        arr[i] = arr[i].split(',')
        arr[i][1] = arr[i][1].replace('T', ' ')[:arr[i][1].rfind('.')]
        arr[i][1] = datetime.strptime(arr[i][1], arrFormat)

        arr[i][2] = arr[i][2].replace('T', ' ')[:arr[i][2].rfind('.')]
        arr[i][2] = datetime.strptime(arr[i][2], arrFormat)

        unix1 = int(time.mktime(arr[i][1].timetuple())) // 100
        unix2 = int(time.mktime(arr[i][2].timetuple())) // 100


        st = str(unix1) + "," + str(unix2)

        if arr[i][0] in dict:
            dict.update({arr[i][0]:dict[arr[i][0]] + "," + st})
        else:
            dict.update({arr[i][0]:st})
       
    return dict

def searchId(dict, StartTime, EndTime):
    ids = set()
    closed = set()

    for i in dict:
        arr = dict[str(i)].split(",")
        for val in range(0, len(arr), 2):
          
            if (i in closed) or check([int(arr[val]), int(arr[val + 1])], StartTime, EndTime):
                closed.add(i)
                if val in ids:
                    ids.remove(i)
            else:
                ids.add(i)
    
    return ids

def findId(dict, ResourseID, StartTime, EndTime):
    arr = dict[str(ResourseID)].split(",")

    for i in range(0, len(arr), 2):
        if (check([int(arr[i]), int(arr[i + 1])], StartTime, EndTime)):
            return False

    return True   




def check(hotel, dateStart, dateEnd):
  return (
    (hotel[0] <= dateStart < hotel[1]) 
    or  
    (hotel[0] < dateEnd <= hotel[1]) 
    or
    (dateStart <= hotel[0] < dateEnd) 
    or
    (dateStart < hotel[1] <= dateEnd)
  )




if __name__ == "__main__":
    val = main()
    findId(val[0], val[1], val[2], val[3])
    print(timeit.timeit("findId(val[0], val[1], val[2], val[3])", setup="from __main__ import findId  , val", number=1))

