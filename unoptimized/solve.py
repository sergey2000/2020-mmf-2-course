from datetime import datetime
import timeit

arrFormat = '%Y-%m-%d %H:%M:%S'


def findId(arr, ResourseID, StartTime, EndTime):
    flag = True
    
    for i in range(1, len(arr)):
        if arr[i][0] == ResourseID:
            if (
                arr[i][1] <= StartTime < arr[i][2]
                or arr[i][1] < EndTime <= arr[i][2]
                or (StartTime <= arr[i][1] and arr[i][2] <= EndTime)
            ):
                flag = False
                break
    
    print(len(arr))

    return flag



def searchID(arr, StartTime, EndTime):
    ID = {}

    for i in range(1, len(arr)):
        if not (
                arr[i][1] <= StartTime < arr[i][2]
                or arr[i][1] < EndTime <= arr[i][2]
                or (StartTime <= arr[i][1] and arr[i][2] <= EndTime)
            ):
            if arr[i][0] not in ID:
                ID.update({arr[i][0]: 1})
        else: ID.update({arr[i][0]: -1})
    
    for key in ID:
        if (not (ID[key] == -1)):
            # print(key)
            continue

    return ID

file = "out100.csv"
        
def readingarr(file):
    arrFormat = '%Y-%m-%d %H:%M:%S'

    f = open(file, "r")

    arr = []

    for line in f.readlines():
        arr.append(line)

    arr[0] = arr[0][:-1]
    arr[0] = arr[0].split(',')

    for i in range(1, len(arr)):
        arr[i] = arr[i].split(',')
        arr[i][1] = arr[i][1].replace('T', ' ')[:arr[i][1].rfind('.')]
        arr[i][1] = datetime.strptime(arr[i][1], arrFormat)

        arr[i][2] = arr[i][2].replace('T', ' ')[:arr[i][2].rfind('.')]
        arr[i][2] = datetime.strptime(arr[i][2], arrFormat)
        # print(arr[i])
    
    f.close()

    return arr 



def main():

    # ResourseID = input("ID: ")
    # print(f'Введите начало и конец даты в формате "{arrFormat}"')
    # StartTime = str(input("Начало: "))
    # EndTime = str(input("Конец: "))

    ResourseID = "532"
    StartTime = "2022-02-06 21:00:00"
    EndTime = "2023-03-01 21:00:00"

    StartTime = datetime.strptime(StartTime, arrFormat)
    EndTime = datetime.strptime(EndTime, arrFormat)

    arr = readingarr(file)

    # if findId(arr, ResourseID, StartTime, EndTime):
    #     print("ID свободен")
    # else:
    #     print("ID занят")

    # print("Поиск свободных ID по заданному времени: ")
    # ans = searchID(arr, StartTime, EndTime)

    # for key in ans:
    #     if (not (ans[key] == -1)):
    #         print(key)

    return [arr, ResourseID, StartTime, EndTime]


    


if __name__ == "__main__":
    val = main()
    searchID(val[0], val[2], val[3])
    print(timeit.timeit("searchID(val[0], val[2], val[3])", setup="from __main__ import searchID  , val", number=1))